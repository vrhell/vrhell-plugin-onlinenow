<?php if (!defined('APPLICATION')) exit();
/**
* Renders a list of users who are taking part in a particular discussion.
*/
class OnlineNowModule extends Gdn_Module {

	protected $_OnlineUsers;

	public function __construct($Sender = '') {
		parent::__construct($Sender);
	}

	public function GetData($Invisible = FALSE) {
		$SQL = Gdn::SQL();
		// $this->_OnlineUsers = $SQL
		// insert or update entry into table
		$Session = Gdn::Session();
		
		if($Session->User)
        	$Photo = $Session->User->Photo;
		
		$Invisible = ($Invisible ? 1 : 0);

		if ($Session->UserID)
			$SQL->Replace('OnlineNow', array(
				'UserID' => $Session->UserID,
				'Timestamp' => Gdn_Format::ToDateTime(),
				'Photo'=> $Session->User->Photo,
				'Invisible' => $Invisible),
				array('UserID' => $Session->UserID)
			);     

		$Frequency = C('Plugins.OnlineNow.Frequency', 4);
		$History = time() - $Frequency;

		$SQL
			->Select('u.UserID, u.Name, u.Photo,u.Email, w.Timestamp, w.Invisible')
			->From('OnlineNow w')
			->Join('User u', 'w.UserID = u.UserID')
			->Where('w.Timestamp >=', date('Y-m-d H:i:s', $History))
			->OrderBy('u.Name');

		if (!$Session->CheckPermission('Plugins.OnlineNow.ViewHidden'))
			$SQL->Where('w.Invisible', 0);

		$this->_OnlineUsers = $SQL->Get();
	}
    
 	public function GetPhoto($User, $Options = array()) {
      if (is_string($Options))
         $Options = array('LinkClass' => $Options);

      if ($Px = GetValue('Px', $Options))
         $User = UserBuilder($User, $Px);
      else
         $User = (object)$User;

      $LinkClass = ConcatSep(' ', GetValue('LinkClass', $Options, ''), 'PhotoWrap');
      $ImgClass = GetValue('ImageClass', $Options, 'ProfilePhoto');

      $Size = GetValue('Size', $Options);
      if ($Size) {
         $LinkClass .= " PhotoWrap{$Size}";
         $ImgClass .= " {$ImgClass}{$Size}";
      } else {
         $ImgClass .= " {$ImgClass}Medium"; // backwards compat
      }

      $FullUser = Gdn::UserModel()->GetID(GetValue('UserID', $User), DATASET_TYPE_ARRAY);
      $UserCssClass = GetValue('_CssClass', $FullUser);
      if ($UserCssClass)
         $LinkClass .= ' '.$UserCssClass;

      $LinkClass = $LinkClass == '' ? '' : ' class="'.$LinkClass.'"';

      $Photo = GetValue('Photo', $User);
      $Name = GetValue('Name', $User);
      $Title = htmlspecialchars(GetValue('Title', $Options, $Name));

      if ($FullUser && $FullUser['Banned']) {
         $Photo = C('Garden.BannedPhoto', 'http://cdn.vanillaforums.com/images/banned_large.png');
         $Title .= ' ('.T('Banned').')';
      }

      if (!$Photo && function_exists('UserPhotoDefaultUrl'))
         $Photo = UserPhotoDefaultUrl($User, $ImgClass);

      if ($Photo) {
         if (!isUrl($Photo)) {
            $PhotoUrl = Gdn_Upload::Url(ChangeBasename($Photo, 'n%s'));
         } else {
            $PhotoUrl = $Photo;
         }
         $Href = Url(UserUrl($User));
         return Img($PhotoUrl, array('alt' => htmlspecialchars($Name), 'class' => $ImgClass));
      } else {
         return '';
      }
	}

   	public function AssetTarget() {
		return 'Panel';
	}

	public function ToString() {
		$String = '';
		$Session = Gdn::Session();
		ob_start();
		?>

			<div id="OnlineNow" class="Box">
				<h5><font color="#3E3F3A"><b> <?php echo T("Online Now");?>: <?php echo $this->_OnlineUsers->NumRows(); ?></b></font></h5>
				<ul class="PanelInfo">
				 	<?php
				  		if ($this->_OnlineUsers->NumRows() > 0) { 
							foreach($this->_OnlineUsers->Result() as $User) {
					?>
					<li>
		 				<?php 
		 					if($User->Invisible == 0)
                                echo '<a class="'.'Name'.'" href="'.Url(UserUrl($User)).'"'.'>'.$this->GetPhoto($User)."&nbsp&nbsp&nbsp".htmlspecialchars($User->Name).'</a>';
                        ?>
					</li>
					<?php 
							} 
						} 
					?>
				</ul>
			</div>
		<?php
		$String = ob_get_contents();
		@ob_end_clean();
		return $String;
	}
}
